export class PersonaResponse {
    idPersona       : number = 0;
    nombre          : string = "";
    apellido        : string = "";
    direccion       : string = "";
    telefono        : string = "";
    fechaNacimiento : string = "";
    correo          : string = "";
    idTipoDocumento : number = 0;
    nroDocumento    : string = "";
    nombreCompleto  : string = "";
}