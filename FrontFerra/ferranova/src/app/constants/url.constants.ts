export const dominio = "https         ://localhost      :7171/";


export const urlConstants = {

    cargo         : dominio + "api/Cargo/",
    rol           : dominio + "api/Rol/",
    usuario       : dominio + "api/usuario/",
    persona       : dominio + "api/Persona/",
    producto      : dominio + "api/Producto/",
    venta         : dominio + "api/Venta/",
    detalleVenta  : dominio + "api/DetalleVenta/",

}

